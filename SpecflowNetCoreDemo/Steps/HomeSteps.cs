﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SpecflowNetCoreDemo.Steps
{
    [Binding]
    public sealed class HomeSteps
    {
        IWebDriver Driver = new ChromeDriver();


        [Given(@"I navigate to URl")]
        public void GivenINavigateToURl()
        {
            Driver.Navigate().GoToUrl("https://www.seleniumeasy.com/test/");
        }

        [Given(@"I click to Input form")]
        public void GivenIClickToInputForm()
        {
            Driver.FindElement(By.XPath("//*[@id='navbar-brand-centered']/ul[1]/li[1]/a")).Click();
        }

        //[Given(@"I click to Simple demo form")]
        //public void GivenIClickToSimpleDemoForm()
        //{
        //    Driver.FindElement(By.XPath("//*[@id='navbar-brand-centered']/ul[1]/li[1]/ul/li[1]/a")).Click();
        //}


        [Then(@": I can see Simple demo form")]
        public void ThenICanSeeSimpleDemoForm()
        {
            IWebElement headingText = Driver.FindElement(By.XPath("//*[@id='easycont']/div/div[2]/div[1]/div[1]"));
            string text = headingText.Text;

            Console.WriteLine("Text : " + text);
            Assert.IsTrue(text.Contains("Single"));
        }

        [Given(@": I click to Simple demo form")]
        public void GivenIClickToSimpleDemoForm()
        {
            Driver.FindElement(By.XPath("//*[@id='navbar-brand-centered']/ul[1]/li[1]/ul/li[1]/a")).Click();
        }


        [Given(@"I enter ""(.*)"" in input")]
        public void GivenIEnterInInput(string Msg)
        {        
            IWebElement input =  Driver.FindElement(By.XPath("//*[@id='user-message']"));
            input.SendKeys(Msg);

        }

        [When(@"I click to Show Message Button")]
        public void WhenIClickToShowMessageButton()
        {
            Driver.FindElement(By.XPath("//*[@id='get-input']/button")).Click();
        }

        [Then(@": I can see ""(.*)""")]
        public void ThenICanSee(string msg)
        {
            string displaymsg = Driver.FindElement(By.XPath("//*[@id='display']")).Text.ToString();
            Console.WriteLine("Msg : " + displaymsg);
            Assert.IsTrue(displaymsg.Contains(msg));
            Driver.Close();
        }

        #region FillInputForm

        //[Given(@"User is on the input form Page")]
        //public void GivenUserIsOnTheInputFormPage()
        //{
        //    Task.Delay(5000).Wait();
        //    Console.WriteLine("User is on the page");
        //    Driver.FindElement(By.XPath("//*[@id='at-cv-lightbox-close']")).Click();
        //    Driver.FindElement(By.XPath("//*[@id='navbar-brand-centered']/ul[1]/li[1]/ul/li[1]/a")).Click();

        //}

        //[When(@"User enter (.*) in input")]
        //public void WhenUserEnterInInput(string yow)
        //{
        //    Driver.FindElement(By.XPath("//*[@id='user-message']")).SendKeys(yow);
        //}

        //[When(@"User click to Show button")]
        //public void WhenUserClickToShowButton()
        //{
        //    Driver.FindElement(By.XPath("//*[@id='get-input']/button")).Click();
        //}

        //[Then(@"User can see the text")]
        //public void ThenUserCanSeeTheText()
        //{
        //    string msg = Driver.FindElement(By.XPath("//*[@id='display']")).Text.ToString();
        //    Console.WriteLine("MSG : " + msg);
        //}



        #endregion

        #region TableScenario

        //[Given(@"User is on the input form Page")]
        //public void GivenUserIsOnTheInputFormPage()
        //{
        //    Task.Delay(5000).Wait();
        //    Console.WriteLine("User is on the page");
        //    Driver.FindElement(By.XPath("//*[@id='at-cv-lightbox-close']")).Click();
        //    Driver.FindElement(By.XPath("//*[@id='navbar-brand-centered']/ul[1]/li[1]/ul/li[1]/a")).Click();
        //}

        //[When(@"User enter Msg in input")]
        //public void WhenUserEnterMsgInInput(Table table)
        //{
        //    var dictionary = new Dictionary<string, string>();
        //    foreach (var row in table.Rows)
        //    {
        //        dictionary.Add(row[0], row[1]);
        //        Console.WriteLine(dictionary["Yow What$$$uppp!?!"]);
        //    }

        //    foreach (TableRow row in table.Rows)
        //    {
        //        foreach (string value in row.Values)
        //        {
        //            Driver.FindElement(By.XPath("//*[@id='user-message']")).SendKeys(value);
        //            Driver.FindElement(By.XPath("//*[@id='get-input']/button")).Click();
        //            Driver.FindElement(By.XPath("//*[@id='user-message']")).Clear();
        //            Task.Delay(3000);
        //        }
        //    }
        //}

        //[When(@"User click to Show button")]
        //public void WhenUserClickToShowButton()
        //{
        //    Driver.FindElement(By.XPath("//*[@id='get-input']/button")).Click();
        //}

        //[Then(@"User can see the text")]
        //public void ThenUserCanSeeTheText()
        //{
        //    string msg = Driver.FindElement(By.XPath("//*[@id='display']")).Text.ToString();
        //    Console.WriteLine("MSG : " + msg);
        //}



        #endregion

        #region TableParams

        [Given(@"User is on the input form Page")]
        public void GivenUserIsOnTheInputFormPage()
        {
            Task.Delay(5000).Wait();
            Console.WriteLine("User is on the page");
            Driver.FindElement(By.XPath("//*[@id='at-cv-lightbox-close']")).Click();
            Driver.FindElement(By.XPath("//*[@id='navbar-brand-centered']/ul[1]/li[1]/ul/li[1]/a")).Click();
        }


        [When(@"User enter ""(.*)"" in input")]
        public void WhenUserEnterInInput(string msg)
        {
            ScenarioContext.Current.Add("msg", msg);
            Driver.FindElement(By.XPath("//*[@id='user-message']")).SendKeys(msg);
        }

        [When(@"User click to Show button")]
        public void WhenUserClickToShowButton()
        {
            Driver.FindElement(By.XPath("//*[@id='get-input']/button")).Click();
            
        }

        [Then(@"User can see the text")]
        public void ThenUserCanSeeTheText()
        {
            string msg = ScenarioContext.Current["msg"].ToString();
            string browsertext = Driver.FindElement(By.XPath("//*[@id='display']")).Text.ToString();
            Console.WriteLine("msg coming form the above step : " + msg);
            Assert.IsTrue(msg.Contains(browsertext));
            //Task.Delay(5000).Wait();
        }




        #endregion
    }
}
