﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.PhantomJS;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace LearningSelenium
{
    [TestClass]
    public class FirstTestClass
    {

        [TestMethod]
        public void TwitterRoundUp()
        {
            IWebDriver driver = new FirefoxDriver();
            try
            {              
                driver.Navigate().GoToUrl("https://www.twitter.com/login");
                driver.Manage().Window.Maximize();
                driver.FindElement(By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[1]/label/div/div[2]/div/input")).SendKeys("rlcalayag");
                driver.FindElement(By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[2]/label/div/div[2]/div/input")).SendKeys("password");
                driver.FindElement(By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[3]/div/div/span/span")).Click();

                Thread.Sleep(2000);

                //after login, composing a tweet
                //driver.FindElement(By.XPath("/html/body/div/div/div/div[2]/main/div/div/div/div[1]/div/div[2]/div/div[2]/div[1]/div/div/div/div[2]/div[1]/div/div/div/div/div/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div"))
                //    .SendKeys("Selenium 8th video recording at:" + DateTime.Now.ToShortTimeString());
                //driver.FindElement(By.XPath("/html/body/div/div/div/div[2]/main/div/div/div/div[1]/div/div[2]/div/div[2]/div[1]/div/div/div/div[2]/div[4]/div/div/div[2]/div/div/span/span"))
                //    .Click();
                //Thread.Sleep(3000);

                //IWebElement ActionContainer = driver.FindElement(By.Id("global-actions"));
                //ReadOnlyCollection<IWebElement> GlobalActions = ActionContainer.FindElements(By.TagName("li"));
                //foreach (IWebElement GlobalAction in GlobalActions)
                //{
                //    string ActionName = GlobalAction.Text;
                //    GlobalAction.Click();
                //    Thread.Sleep(5000);
                //}
            
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: " + ex.ToString());
            }
            finally
            {
                driver.Close();
                driver.Quit();
            }
        }

        [TestMethod]
        public void SelectedRadioButton()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.facebook.com/");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.Id("u_0_a")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("u_0_b")).Click();
            Thread.Sleep(2000);
            driver.Close();
            driver.Quit();
        }

        [TestMethod]
        public void SelectedCheckButton()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.twitter.com/signup");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.ClassName("use-cookie-personalization-field")).Click();
            Thread.Sleep(2000);
            driver.Close();
            driver.Quit();
        }


        [TestMethod]
        public void WikiSearch()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.wikipedia.org/");
            driver.Manage().Window.Maximize();

            
            List<string> CentralLanguages = new List<string>();
            ReadOnlyCollection<IWebElement> languages = driver.FindElements(By.ClassName("central-featured-lang"));
            foreach (IWebElement language in languages)
            {
                string lang = language.Text;
                lang = lang.Substring(0, lang.LastIndexOf("\r"));
                CentralLanguages.Add(lang);
            }
            string stop = "";

            SelectElement  selectLanguage = new SelectElement(driver.FindElement(By.Id("searchLanguage")));
            selectLanguage.SelectByText("Deutsch");
            selectLanguage.SelectByValue("be");
            selectLanguage.SelectByIndex(0);

            //#region
            //List<String> textofanchors = new List<string>();
            //ReadOnlyCollection<IWebElement> anchorLists = driver.FindElements(By.TagName("a"));
            //foreach (IWebElement anchor in anchorLists)
            //{
            //    if(anchor.Text.Length > 0)
            //    {
            //        if(anchor.Text.Contains("English"))
            //        {
            //            textofanchors.Add(anchor.Text);
            //            //anchor.Click();
            //        }
            //    }
                    
            //}
            //string stop = "";

            ////driver.FindElement(By.Name("search")).SendKeys("Emilia Tan");
            ////driver.FindElement(By.XPath("//*[@id='search-form']/fieldset/button")).Click();
            //#endregion


            driver.Quit();
        }


        [TestMethod]
        public void ChromeMethod()
        {
            string ActualResult;
            string ExpectedResult = "Bing";

            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.google.com.ph/");
            driver.Manage().Window.Maximize();

            ActualResult = driver.Title;
            if(ActualResult.Contains(ExpectedResult))
            {
                Console.WriteLine("Test Case Passed");
                Assert.IsTrue(true, "Test Case Passed");
            }
            else
            {
                Console.WriteLine("Test Case Failed");
            }

            driver.Close();
            driver.Quit();
        }

        [TestMethod]
        public void FirefoxMethod()
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://www.google.com.ph/");
            driver.Manage().Window.Maximize();
            driver.Close();
            driver.Quit();
        }

        //[TestMethod]
        //public void IEMethod()
        //{
        //    IWebDriver driver = new InternetExplorerDriver();
        //    driver.Navigate().GoToUrl("https://www.google.com.ph/");
        //    driver.Manage().Window.Maximize();
        //    driver.Close();
        //    driver.Quit();
        //}

        //[TestMethod]
        //public void PhantomJSMethod()
        //{
        //    IWebDriver driver = new PhantomJSDriver();
        //    driver.Navigate().GoToUrl("https://www.google.com.ph/");
        //    driver.Manage().Window.Maximize();
        //    driver.Close();
        //    driver.Quit();
        //}
    }
}
