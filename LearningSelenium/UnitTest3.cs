﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Collections.ObjectModel;
using System.Threading;

namespace LearningSelenium
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void VotePollsInMSN()
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://www.msn.com/en-us/news/polls");
            driver.Manage().Window.Maximize();
            ReadOnlyCollection<IWebElement> Polls = driver.FindElements(By.ClassName("csw-question"));
            int counter = 1;
            foreach (IWebElement Poll in Polls)
            {
                //ToDo Operation
                Random rnd = new Random();
                int OptionNumber = rnd.Next(1, 4);

                string questionID = Poll.FindElement(By.ClassName("csw-q-rdo-txt"))
                    .GetAttribute("id");
                string RandomAnswer = "csw-q-rdo-135066-1412864";
                Poll.FindElement(By.Id(RandomAnswer)).Click();
                Poll.FindElement(By.ClassName("csw-btn csw-btn-primary csw-btn-submit outline")).Click();
                Thread.Sleep(2000);
                //if(counter < Polls.Count)
                //{

                //}    
            }
        }


        [TestMethod]
        public void TestAspnetawesome()
        {
            IWebDriver driver = new ChromeDriver();

            driver.Navigate().GoToUrl("https://demowf.aspnetawesome.com/");

            CustomControl.EnterText(driver.FindElement(By.Id("ContentPlaceHolder1_Meal")), "Mango");
            CustomControl.Click(driver.FindElement(By.XPath("//input[@name='ctl00$ContentPlaceHolder1$ChildMeal1']/following-sibling::div[text()='Celery']")));

            CustomControl.SelectByText(driver.FindElement(By.Id("ContentPlaceHolder1_Add1-awed")), "Cauliflower");

            //CustomControl.ComboBox("ContentPlaceHolder1_AllMealsCombo", "Almonds");

            Assert.IsTrue(true, "Test Case Passed");


        }
    }
}
