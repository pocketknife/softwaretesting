﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningSelenium
{
    [TestClass]
    public class FrameClass
    {
        IWebDriver driver;

        [TestMethod]
        public void TakeScreenshot()
        {
            try
            {
                driver = new FirefoxDriver();
                driver.Navigate().GoToUrl("https://twitter.com/");
                driver.Manage().Window.Maximize();
                ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile("TwitterScreenshot" + ".png", ScreenshotImageFormat.Png);


            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                driver.Quit();
            }
        }
    }
}
