﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LearningSelenium
{
    [TestFixture]
    public class UnitTest1
    {
        public IWebDriver driver;

        [SetUp]
        public void init()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void NavigateToGoogle()
        {
            driver.Navigate().GoToUrl("https://www.google.com.ph/");
            driver.Manage().Window.Maximize();
            System.Threading.Thread.Sleep(3000);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}
