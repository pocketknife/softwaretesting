﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningSelenium
{
    public class LoginPage
    {
        IWebDriver driver;
        By Username; //= By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[1]/label/div/div[2]/div/input");
        By Password;//= By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[2]/label/div/div[2]/div/input");
        By login_button; //= By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[3]/div/div/span/span");

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
            Username = By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[1]/label/div/div[2]/div/input");
            Password = By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[2]/label/div/div[2]/div/input");
            login_button = By.XPath("/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[3]/div/div/span/span");
        }

        public void TypeUsername()
        {
            driver.FindElement(Username).SendKeys("rlcalayag");
        }

        public void TypePassword()
        {
            driver.FindElement(Password).SendKeys("password");
        }

        public void ClickOnLoginButton()
        {
            driver.FindElement(login_button).Click();
        }
    }
}
