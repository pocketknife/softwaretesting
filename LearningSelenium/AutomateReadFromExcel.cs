﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace LearningSelenium
{
    [TestClass]
    public class AutomateReadFromExcel
    {
        static ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("MutipleLoginPHPTravels_" +
            DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss").Trim() + ".html");
        static ExtentReports extent = new ExtentReports();
        static int counter = 1;
        IWebDriver driver;

        [TestMethod]
        [Description("This method will try to read multiple user credentials from Excel and attempt to login with each one.")]
        public void LoginAttemptPHPTravels()
        {
            try
            {
                extent.AttachReporter(htmlReporter);
                extent.AddSystemInfo("Operating System: ", "Windows 10");
                extent.AddSystemInfo("HostName: ", "RLawrence Laptop");
                extent.AddSystemInfo("Browser: ", "Google Chrome");
                driver = new ChromeDriver();
                driver.Navigate().GoToUrl("https://phptravels.net/login");
                driver.Manage().Window.Maximize();
            }
            catch(Exception ex)
            {

            }
        }
    }
}
