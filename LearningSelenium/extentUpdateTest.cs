﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework.Internal;
using System.Net;

namespace LearningSelenium
{
    [TestClass]
    public class ExtentUpdateTest
    {
        static ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentUpdated.html");
        static ExtentReports extent = new ExtentReports();
        IWebDriver driver;

        [TestInitialize]
        public void Setup()
        {
            extent.AttachReporter(htmlReporter);
        }

        public static string Capture(IWebDriver driver, string ScreenShotName)
        {
            ITakesScreenshot ts = (ITakesScreenshot)driver;
            Screenshot screenshot = ts.GetScreenshot();
            string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string uptobinPath = path.Substring(0, path.LastIndexOf("bin")) + "Screenshots\\" + ScreenShotName + ".png";
            string localPath = new Uri(uptobinPath).LocalPath;
            screenshot.SaveAsFile(localPath, ScreenshotImageFormat.Png);

            return localPath;
        }

        [TestMethod]
        public void EnVariables()
        {
            //use real value
            string hostname = Dns.GetHostName();
            OperatingSystem os = Environment.OSVersion;

            extent.AddSystemInfo("Operating System", os.ToString());
            extent.AddSystemInfo("HostName: ", hostname);
            extent.AddSystemInfo("Browser: ", "Google Chrome");
        }

        [TestMethod]
        public void PassedTestMethod()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://twitter.com/home");

            //Screenshot
            string screenshotpath = Capture(driver, "TwitterSsh");

            var test = extent.CreateTest("<div style='color:green;font -weight : bold'>PassedTestMethod</div>", "<h3>This test method gets passed.</h3>");
            test.Log(Status.Info, "First step of PassedTestMethod");
            test.Pass("<div style='color:green;font -weight : bold'>PassedTestMethod gets completed.</div>");
            test.AddScreenCaptureFromPath(screenshotpath, "Twitter landingpage screenshot");

            driver.Quit();
        }

        [TestMethod]
        public void FailedTestMethod()
        {
            //Screenshot
            var test = extent.CreateTest("<div style='color:red;font -weight : bold'>FailedTestMethod</div>", "This test method gets failed.");
            test.Log(Status.Info, "First step of FailedTestMethod");
            test.Fail("<div style='color:red;font -weight : bold'>FailedTestMethod gets completed.</div>");
        }

        [TestMethod]
        public void SkippedTestMethod()
        {
            //Screenshot
            var test = extent.CreateTest("<div style='color:#808080;font -weight : bold'>SkippedTestMethod", "This test method gets skipped.</div>");
            test.Log(Status.Info, "First step of SkippedTestMethod");
            test.Skip("<div style='color:#808080;font -weight : bold'>SkippedTestMethod gets completed.</div>");
        }

        [TestMethod]
        public void WarningTestMethod()
        {
            //Screenshot
            var test = extent.CreateTest("<div style='color:#FFA500;font -weight : bold'>WarningTestMethod", "This test method gets warned.</div>");
            test.Log(Status.Info, "First step of WarningTestMethod");
            test.Warning("<div style='color:#FFA500;font -weight : bold'>WarningTestMethod gets completed.</div>");
        }

        [TestCleanup]
        public void Cleanup()
        {
            extent.Flush();
        }
    }
}
