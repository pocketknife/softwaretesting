﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LearningSelenium
{
    [TestClass]
    public class TwitterLoginPF
    {
        [TestMethod]
        public void verifyTwitterLogin()
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.twitter.com/login");
            LoginPagePF loginPF = new LoginPagePF();
            PageFactory.InitElements(driver, loginPF);
            loginPF.Username.SendKeys("rlcalayag");
            loginPF.Password.SendKeys("password");
            loginPF.ClickOnLoginButton();
            Thread.Sleep(3000);
            driver.Quit();

        }
    }
}
