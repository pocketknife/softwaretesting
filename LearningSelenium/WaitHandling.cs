﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace LearningSelenium
{
    [TestClass]
    public class WaitHandling
    {
        IWebDriver driver;

        [TestMethod]
        public void TestMethodForImplicitWait()
        {
            try
            {
                driver = new FirefoxDriver();
                driver.Navigate().GoToUrl("https://www.google.com.ph/");
                driver.Manage().Window.Maximize();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
                string WaitText = driver.FindElement(By.Id("112233")).Text;
                string WaitText2nd = driver.FindElement(By.Id("778899")).Text;
            }
            catch(Exception ex)
            {
                Assert.Fail("Test method for ImplicitWait failed ==> " + ex.Message);
            }
            finally
            {
                driver.Quit();
            }
        }

        [TestMethod]
        public void TestMethodForExplicitWait()
        {
            try
            {
                driver = new FirefoxDriver();
                driver.Navigate().GoToUrl("https://www.google.com.ph/");
                driver.Manage().Window.Maximize();
                IWebElement ele = ExplicitWait(driver, "112233");
                if(ExplicitWait(driver, "112233") != null)
                {
                    string TagNameOfElement = ExplicitWait(driver, "112233").TagName;
                    Assert.IsTrue(true);
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.Fail("TestMethodForExplicitWait Failed ==> " + ex.Message);
            }
            finally
            {
                driver.Quit();
            }
        }

        public static IWebElement ExplicitWait(IWebDriver driver, string Identifier)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(3)).
                Until(ExpectedConditions.ElementExists(By.Id(Identifier)));
        }
    }
}
