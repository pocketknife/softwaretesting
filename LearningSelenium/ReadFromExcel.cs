﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Diagnostics;

namespace LearningSelenium
{
    [TestClass]
    public class ReadFromExcel
    {
        static ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("MultipleLoginPHPTravels_" +
            DateTime.Now.ToString("yyy-dd-M-HH-mm-ss").Trim() + ".html");
        static ExtentReports extent = new ExtentReports();
        static int counter = 1;
        IWebDriver driver;

        [TestMethod]
        [Description("This method will try to read multiple user credentials from Excel and attemt to login with each one")]
        public void LoginAttemptPHPTravels()
        {
            try
            {
                extent.AttachReporter(htmlReporter);
                extent.AddSystemInfo("Operating System: ", "Windows 10");
                extent.AddSystemInfo("HostName: ", "ReyanldLawrence Laptop");
                extent.AddSystemInfo("Browser: ", "Google Chrome");
                driver = new ChromeDriver();
                driver.Navigate().GoToUrl("https://phptravels.net/login");
                driver.Manage().Window.Maximize();
                ExcelOperations.PopulateInCollection(@"C:\SoftwareTestings\softwaretesting\LearningSelenium\Data\Travels1.xlsx");
                for(int i = 1; i <= ExcelOperations.GetTotalRowCount(); i++)
                {
                    ExcelFun(driver, i);
                }
;            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }
            finally
            {
                extent.Flush();
                driver.Quit();
            }
        }

        public void ExcelFun(IWebDriver driver, int rowNum)
        {
            try
            {
                var test = extent.CreateTest("Test Number : " + counter);
                string CurrentUsername = ExcelOperations.ReadData(rowNum, "UserName");
                string CurrentPassword = ExcelOperations.ReadData(rowNum, "Password");
                Util.ExplicitWaitByName(driver, "username").SendKeys(CurrentUsername);
                Util.ExplicitWaitByName(driver, "password").SendKeys(CurrentPassword);
                Util.ExplicitWaitByXpath(driver, "//button[.='Login']").Click();
                test.Log(Status.Info, "Attempting to log in . .. ... ...");
                if(Util.IfElementExistsByClassName(driver, "profile-tabs"))
                {
                    if(Util.ExplicitWaitAndWaitPageLoadByPageElementName(driver, "profile-tabs").ToLower().Contains("my account"))
                    {
                        string screenshotpath = Util.Capture(driver, "ScreenShot" + DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss").Trim() + "");
                        test.Pass("Test Number " + counter + " passed and successfully logged in with Username = " +
                            CurrentUsername + " and Password = " + CurrentPassword);
                        test.AddScreenCaptureFromPath(screenshotpath, "Dashboard Screenshot: ");
                        logoutPHPTravel(driver);
                        test.Log(Status.Info, "Attempting to log out . .. ... ....");
                    }
                }
                else
                {
                    string screenshotpath = Util.Capture(driver, "ScreenShot" + DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss").Trim() + "");
                    test.Fail("Test Number " + counter + "failed, Unable to log in with Username= " + CurrentUsername + " and Password = " +
                        CurrentPassword + "");
                    test.AddScreenCaptureFromPath(screenshotpath, "Login Page Screenshot:");
                    driver.Navigate().GoToUrl("https://phptravels.net/login");
                    Util.ExplicitWaitAndWaitPageLoadByPageElementName(driver, "username");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                counter++;
            }
        }

        public void logoutPHPTravel(IWebDriver driver)
        {
            Util.ExplicitWaitByXpath(driver, "/html/body/nav/div/div[2]/ul[2]/ul/li[1]/a").Click();
            Util.ExplicitWaitByXpath(driver, "/html/body/nav/div/div[2]/ul/li[1]/ul/li[2]/a").Click();
            Util.IfElementExistsByClassName(driver, "form-group");
            Util.ExplicitWaitAndWaitPageLoadByPageElementName(driver, "username");
        }

        [TestMethod]
        public void ReadingDataFromExcelTestMethod()
        {
            try
            {
                ExcelOperations.PopulateInCollection(@"C:\SoftwareTestings\softwaretesting\LearningSelenium\Data\Dbx.xlsx");
                Debug.WriteLine("*******************");
                Debug.WriteLine("First Person Name is: " + ExcelOperations.ReadData(1,"PName"));
                Debug.WriteLine("First Person's Balance is: " + ExcelOperations.ReadData(1, "Balance"));
                Debug.WriteLine("First Person's Amount is: " + ExcelOperations.ReadData(1, "Amount"));
                Debug.WriteLine("*******************");
                Debug.WriteLine("Second Person Name is: " + ExcelOperations.ReadData(2, "PName"));
                Debug.WriteLine("Second Person's Balance is: " + ExcelOperations.ReadData(2, "Balance"));
                Debug.WriteLine("Second Person's Amount is: " + ExcelOperations.ReadData(2, "Amount"));
                Debug.WriteLine("*******************");
                Debug.WriteLine("Third Person Name is: " + ExcelOperations.ReadData(3, "PName"));
                Debug.WriteLine("Third Person's Balance is: " + ExcelOperations.ReadData(3, "Balance"));
                Debug.WriteLine("Third Person's Amount is: " + ExcelOperations.ReadData(3, "Amount"));
                Debug.WriteLine("*******************");
                Debug.WriteLine("Fourth Person Name is: " + ExcelOperations.ReadData(4, "PName"));
                Debug.WriteLine("Fourth Person's Balance is: " + ExcelOperations.ReadData(4, "Balance"));
                Debug.WriteLine("Fourth Person's Amount is: " + ExcelOperations.ReadData(4, "Amount"));
                Debug.WriteLine("*******************");

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }     
        }
    }
}
