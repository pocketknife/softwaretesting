﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support;
using NUnit.Framework;

namespace LearningSelenium
{
    [TestFixture]
    [Parallelizable]
    public class NUnitParallelTest : Base
    {
        [Test]
        public void SearchGoogle()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://www.google.com.ph/");
            driver.FindElement(By.XPath("/html/body/div[2]/div[2]/form/div[2]/div[1]/div[1]/div/div[2]/input"))
                .SendKeys("Emilia Tan");
            driver.FindElement(By.XPath("/html/body/div[2]/div[2]/form/div[2]/div[1]/div[3]/center/input[1]"))
                .Click();
            System.Threading.Thread.Sleep(2000);

        }

        [Test]
        public void SearchGoogle1A()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://www.google.com.ph/");
            driver.FindElement(By.XPath("/html/body/div[2]/div[2]/form/div[2]/div[1]/div[1]/div/div[2]/input"))
                .SendKeys("Elizabeth Liones");
            driver.FindElement(By.XPath("/html/body/div[2]/div[2]/form/div[2]/div[1]/div[3]/center/input[1]"))
                .Click();
            System.Threading.Thread.Sleep(2000);

        }
    }

    [TestFixture]
    [Parallelizable]
    public class NUnitParallelTest2 : Base
    {
        [Test]
        public void SearchGoogle2()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://www.google.com.ph/");
            driver.FindElement(By.XPath("/html/body/div[2]/div[2]/form/div[2]/div[1]/div[1]/div/div[2]/input"))
                .SendKeys("Lucy Heartfilia");
            driver.FindElement(By.XPath("/html/body/div[2]/div[2]/form/div[2]/div[1]/div[3]/center/input[1]"))
                .Click();
            System.Threading.Thread.Sleep(2000);

        }
    }
}
