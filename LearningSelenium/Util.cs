﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace LearningSelenium
{
    public class Util
    {
        public static IWebElement FluentWaitByName(IWebDriver driver, string NameLocator)
        {
            try
            {
                DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(driver);
                fluentWait.Timeout = TimeSpan.FromSeconds(3);
                fluentWait.PollingInterval = TimeSpan.FromMilliseconds(250);
                fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = fluentWait.Until(x => x.FindElement(By.Name(NameLocator)));
                return searchElement;

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Some Error: " + ex.Message);
                return null;
            }
            //finally
            //{

            //}
        }

        public static IWebElement ExplicitWaitByName(IWebDriver driver, string NameLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(3));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.Name(NameLocator)));
                return searchElement;
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Some error: " + ex.Message);
                return null;
            }
        }

        public static IWebElement ExplicitWaitByXpath(IWebDriver driver, string XpathLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.XPath(XpathLocator)));
                return searchElement;
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Some Error: " + ex.Message);
                return null;
            }
        }

        public static IWebElement ExplicitWaitById(IWebDriver driver, string IdLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.Id(IdLocator)));
                return searchElement;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Some Error: " + ex.Message);
                return null;
            }
        }

        public static IWebElement ExplicitWaitByCss(IWebDriver driver, string CssLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.Id(CssLocator)));
                return searchElement;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Some Error: " + ex.Message);
                return null;
            }
        }

        public static string ExplicitWaitAndWaitPageLoadByPageElementName(IWebDriver driver, string NameIdentifierOfLoadingPage)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException), typeof(ElementNotVisibleException));
                wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName(NameIdentifierOfLoadingPage)));
                return driver.Title;
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Some Error: " + ex.Message);
                return null;
            }
        }

        public static bool IfElementExistsByClassName(IWebDriver driver, string elementToBeSearched)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException), typeof(ElementNotVisibleException));
                wait.Until(ExpectedConditions.ElementExists(By.ClassName(elementToBeSearched)));

                return true;
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Some Error: " + ex.Message);
                return false;
            }
        }

        public static string Capture(IWebDriver driver, string ScreenShotName)
        {
            ITakesScreenshot ts = (ITakesScreenshot)driver;
            Screenshot screenshot = ts.GetScreenshot();

            screenshot.SaveAsFile(@"C:\SoftwareTestings\softwaretesting\LearningSelenium\Data\" + "SampleScreenshot" +
                ".png", ScreenshotImageFormat.Png);

            return @"C:\SoftwareTestings\softwaretesting\LearningSelenium\Data\" + "SampleScreenshot" + ".png";
        }
            
    }
}
