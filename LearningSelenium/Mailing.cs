﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace LearningSelenium
{
    [TestClass]
    public class Mailing
    {
        [TestMethod]
        public void MethodToSendEmail()
        {
            try
            {
                //Sender's email, Sender's password, To/receiver's email, Subject, body, cc, attachment
                MailMessage mail = new MailMessage();
                string FromEmail = "@gmail.com";
                string Password = "";
                string ToEmail = "@gmail.com";
                string Subject = "Test Subject";
                string contentBody = "<h3>Test Mail from Selenium Tutorial</h3>";

                mail.From = new MailAddress(FromEmail);
                mail.To.Add(ToEmail);
                mail.Subject = Subject;
                mail.Body = contentBody;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.Credentials = new NetworkCredential(FromEmail, Password);
                smtp.EnableSsl = true;
                smtp.Send(mail);


            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Email sent.");
            }
        }

        [TestMethod]
        public void TriggerEmail()
        {
            try
            {
                Assert.AreEqual(1, 2);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                SendAnEmailNow(ex.Message.Trim(), ex.StackTrace);
            }
            finally
            {

            }
        }

        public void SendAnEmailNow(string Subject, string BodyContent)
        {

            //Sender's email, Sender's password, To/receiver's email, Subject, body, cc, attachment
            MailMessage mail = new MailMessage();
            string FromEmail = "@gmail.com";
            string Password = "";
            string ToEmail = "@gmail.com";

            mail.From = new MailAddress(FromEmail);
            mail.To.Add(ToEmail);
            mail.Subject = Subject;
            mail.Body = BodyContent;
            mail.IsBodyHtml = true;
            mail.Attachments.Add(new Attachment(@"C:/SoftwareTestings/softwaretesting/LearningSelenium/bin/Debug/extentUpdated.html"));

            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new NetworkCredential(FromEmail, Password);
            smtp.EnableSsl = true;
            smtp.Send(mail);

        }
    }
}
