﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningSelenium
{
    public class LoginPagePF
    {
        //PageFactory -> Twitter login method
        //FindsBy @FindBy
        //CacheLookup @CacheLookup
        //How

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[1]/label/div/div[2]/div/input")]
        [CacheLookup]
        public IWebElement Username { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[2]/label/div/div[2]/div/input")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div/div/div/div[2]/main/div/div/div[1]/form/div/div[3]/div/div/span/span")]
        public IWebElement Login_button { get; set; }

        public void ClickOnLoginButton()
        {
            Login_button.Click();
        }
    }
}
