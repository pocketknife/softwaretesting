﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Diagnostics;
using System.Threading;

namespace LearningSelenium
{
    [TestClass]
    public class HandlingAlerts
    {
        IWebDriver driver;

        [TestMethod]
        public void ForSimpleAlert()
        {
            try
            {
                driver = new FirefoxDriver();
                driver.Navigate().GoToUrl("https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/");
                driver.Manage().Window.Maximize();
                IWebElement simpleAlertButton = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div[2]/div/div/p[3]/button"));
                simpleAlertButton.Click();
                IAlert simpleAlert = driver.SwitchTo().Alert();
                System.Threading.Thread.Sleep(3000);
                Debug.WriteLine(simpleAlert.Text);
                simpleAlert.Accept();
                driver.SwitchTo().DefaultContent();

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                driver.Quit();
            }
        }

        [TestMethod]
        public void ForConfirmAlert()
        {
            try
            {
                driver = new FirefoxDriver();
                driver.Navigate().GoToUrl("https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/");
                driver.Manage().Window.Maximize();
                IWebElement confirmAlertButton = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div[2]/div/div/p[7]/button"));
                confirmAlertButton.Click();
                IAlert confirmAlert = driver.SwitchTo().Alert();
                System.Threading.Thread.Sleep(3000);
                Debug.WriteLine(confirmAlert.Text);
                confirmAlert.Dismiss();
                driver.SwitchTo().DefaultContent();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                driver.Quit();
            }
        }
    }
}
